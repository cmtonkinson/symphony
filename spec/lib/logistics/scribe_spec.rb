require 'spec_helper'

describe Logistics::Scribe do
  describe '#save' do
    around :each do |example|
      @id = SecureRandom.uuid
      @ob = Realm::TestObj.new @id
      FakeFS do
        example.run
        FakeFS::FileSystem.clear # https://github.com/fakefs/fakefs/issues/343
      end
    end

    it 'creates the parent directory' do
      parent_dir = File.dirname Logistics::Scribe.pathname Realm::TestObj, @id
      expect(File.directory?(parent_dir)).to be false
      Logistics::Scribe.save @ob
      expect(File.directory?(parent_dir)).to be true
    end

    it 'creates the data file' do
      data_file = Logistics::Scribe.pathname Realm::TestObj, @id
      expect(File.readable?(data_file)).to be false
      Logistics::Scribe.save @ob
      expect(File.readable?(data_file)).to be true
    end
  end
end

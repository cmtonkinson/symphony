class JobTester
  attr_accessor :x

  def initialize(val = 0)
    @x = val
  end

  def call_me_maybe
    @x += 10
  end
end

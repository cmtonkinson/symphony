module DoesDamage
  extend Interface

  # Must return an Objekt
  method :whodunnit
end

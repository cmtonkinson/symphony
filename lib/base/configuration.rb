require 'openssl'

module Base
  class Configuration
    attr_accessor :sleep_interval, :recovery_file, :hash_salt, :hash_iterations, :hash_digest

    def initialize
      # Wait time (in seconds) between iterations of the primary server loop.
      @sleep_interval = Symphony.configuration.sleep_interval

      # For account management
      @hash_salt       = ''
      @hash_iterations = 20_000
      @hash_digest     = OpenSSL::Digest.new('SHA512')

      # Where to store the reboot fd => Avatar Hash?
      @recovery_file = Symphony.tmp.join 'recovery.yml'
    end
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield configuration if block_given?
  end
end

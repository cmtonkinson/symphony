module Talent
  module PassiveAbility
    def type
      :passive
    end

    def type_s
      'passive'
    end
  end
end

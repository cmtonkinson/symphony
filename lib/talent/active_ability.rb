module Talent
  module ActiveAbility
    def type
      :active
    end

    def type_s
      'active'
    end
  end
end

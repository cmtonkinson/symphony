require 'fileutils'

module Logistics
  class Scribe
    def self.save(objekt)
      path    = pathname objekt.class, objekt.id
      content = objekt.serialize
      # Ensure the directory exists.
      FileUtils.mkdir_p File.dirname path
      # Write the serialized content to disk.
      File.write(path, content)
    end

    def self.load(klass, objekt_id)
      File.open pathname(klass, objekt_id), 'r' do |fp|
        klass.deserialize fp.read
      rescue StandardError => e
        Symphony.logger.error "Error while loading #{klass}:#{objekt_id} - #{e.message}"
        e.backtrace.each do |line|
          Symphony.logger.debug "  error at #{line}"
        end
        return false
      end
    end

    def self.pathname(klass, objekt_id)
      dirname(klass).join "#{objekt_id}.yml"
    end

    def self.dirname(klass)
      Symphony.data.join klass.name.split('::').last.downcase
    end

    # Returns all zone ids found in the data directory.
    def self.the_story_of_baron_munchausen
      Dir[Symphony.zone.join '*.yml'].map do |zonefile|
        File.basename(zonefile).gsub('.yml', '')
      end
    end
  end
end

module Integration
  class InteractiveMap
    def a
      # Eventually this class will hold state and logic assiciated
      # with the interactive map, feeding data to the InteractiveMapView
      # but for now it just returns 'A' to shut up Rubocop.
      'A'
    end
  end
end
